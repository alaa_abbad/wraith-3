# WELCOME TO WRAITH #

Wraith uses a headless browser to create screenshots of webpages on different environments (or at different moments in time) and then creates a diff of the two images; the affected areas are highlighted in blue.

# HOW TO USE FOR VARDOT #

Please edit the "domains" and "paths" for each project. Submit a pull request and then ask Rajab to generate the report.